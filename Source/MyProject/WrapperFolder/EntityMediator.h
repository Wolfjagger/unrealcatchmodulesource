// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreClasses/IEntityMediator.h"
#include <memory>

class AEntityWrapper;
class Entity;



class EntityMediator : public IEntityMediator {

public:

	EntityMediator(AEntityWrapper& entityWrapper);
	
	// Called from EntityWrapper
	void BeginPlay() const override;
	void Tick(float deltaTime) const override;

	// Called from Entity
	FVector GetLocation() const override;
	void SetLocation(FVector newLocation) const override;
	


private:

	AEntityWrapper& wrapper;

	std::unique_ptr<Entity> pEntity;

};
