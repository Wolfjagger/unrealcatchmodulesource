// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyProject.h"
#include "EntityMediator.h"
#include "EntityWrapper.h"
#include "CoreClasses/Entity.h"



EntityMediator::EntityMediator(AEntityWrapper& entityWrapper)
	: wrapper(entityWrapper),
	pEntity(std::make_unique<Entity>(*this)) {
	
}

// Called from EntityWrapper
void EntityMediator::BeginPlay() const { pEntity->BeginPlay(); }
void EntityMediator::Tick(float deltaTime) const { pEntity->Tick(deltaTime); }

// Called from EntityChar
FVector EntityMediator::GetLocation() const { return wrapper.GetActorLocation(); }
void EntityMediator::SetLocation(FVector newLocation) const { wrapper.SetActorLocation(newLocation); }
