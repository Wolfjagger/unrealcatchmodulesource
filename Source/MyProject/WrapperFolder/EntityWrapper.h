// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include <memory>
#include "EntityMediator.h"
#include "EntityWrapper.generated.h"

class EntityChar;



UCLASS(abstract)
class MYPROJECT_API AEntityWrapper : public AActor {

	GENERATED_BODY()

public:

	// Normal constructor
	AEntityWrapper();

	~AEntityWrapper();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;



private:

	const std::unique_ptr<EntityMediator> pEntityMediator;
	
};
