// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyProject.h"
#include "EntityWrapper.h"
#include <memory>
#include "CoreClasses/Entity.h"



// PUBLIC

AEntityWrapper::AEntityWrapper()
	: pEntityMediator(std::make_unique<EntityMediator>(*this)) {

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

AEntityWrapper::~AEntityWrapper() {

	// EntityMediator automatically deletes
	
}



// Called when the game starts or when spawned
void AEntityWrapper::BeginPlay() {

	Super::BeginPlay();

	pEntityMediator->BeginPlay();
	
}

// Called every frame
void AEntityWrapper::Tick( float DeltaTime ) {

	Super::Tick(DeltaTime);

	pEntityMediator->Tick(DeltaTime);
	
}





// PRIVATE
