// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <memory>

class IEntityMediator;



class MYCOREMODULE_API Entity {

public:

	Entity(IEntityMediator& mediator);
	
	~Entity();

	// Called when the game starts or when spawned
	void BeginPlay();
	
	// Called every frame
	void Tick(float deltaTime);



	// Movement functions
	FVector GetLocation() const;
	void SetLocation(FVector newLoc);


	
private:

	// Owner
	IEntityMediator& entityMediator;

};
