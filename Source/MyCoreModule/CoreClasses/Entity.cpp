// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyCoreModule.h"
#include "Entity.h"
#include "IEntityMediator.h"
#include <memory>



// PUBLIC

Entity::Entity(IEntityMediator& mediator) 
	: entityMediator(mediator) {	
	
}

Entity::~Entity() {

}



// Called by the wrapper when the game starts or when spawned
void Entity::BeginPlay() {
	
}

// Called by the wrapper every frame
void Entity::Tick(float deltaTime) {

}



// Movement functions
FVector Entity::GetLocation() const {
	return entityMediator.GetLocation();
}

void Entity::SetLocation(FVector newLoc) {
	entityMediator.SetLocation(newLoc);
}



// PRIVATE
