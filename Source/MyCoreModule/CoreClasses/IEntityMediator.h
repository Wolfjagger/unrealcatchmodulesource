// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyCoreModule.h"



class MYCOREMODULE_API IEntityMediator {

public:

	// Called from EntityWrapper
	virtual void BeginPlay() const = 0;
	virtual void Tick(float deltaTime) const = 0;

	// Called from Entity
	virtual FVector GetLocation() const = 0;
	virtual void SetLocation(FVector newLocation) const = 0;

};
