// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyTestingModule.h"
#include "EntityMock.h"
#include "CoreClasses/Entity.h"



EntityMock::EntityMock()
	: pEntity(std::make_unique<Entity>(*this)),
	pLocation(std::make_unique<FVector>(FVector::ZeroVector)) {
	
}

// Called from EntityWrapper
void EntityMock::BeginPlay() const { pEntity->BeginPlay(); }
void EntityMock::Tick(float deltaTime) const { pEntity->Tick(deltaTime); }

// Called from EntityChar
FVector EntityMock::GetLocation() const { return *pLocation; }
void EntityMock::SetLocation(FVector newLocation) const { pLocation->Set(newLocation.X, newLocation.Y, newLocation.Z); }
