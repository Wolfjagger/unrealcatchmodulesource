// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreClasses/IEntityMediator.h"
#include <memory>

class Entity;

class EntityMock : public IEntityMediator {

public:

	EntityMock();
	
	void BeginPlay() const override;
	void Tick(float deltaTime) const override;

	// Called from Entity
	FVector GetLocation() const override;
	void SetLocation(FVector newLocation) const override;



private:

	std::unique_ptr<Entity> pEntity;

	std::unique_ptr<FVector> pLocation;

};