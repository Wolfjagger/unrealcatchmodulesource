#pragma once

#include "MyTestingModule.h"
#include "TestEntity.h"
#include "EntityMock.h"
#include "CoreClasses/Entity.h"

TEST_CASE("Entity test.", "[Entity]") {

	std::unique_ptr<EntityMock> pEntityMock = std::make_unique<EntityMock>();

	SECTION("1 == 1") {
		REQUIRE(1 == 1);
	}

	SECTION("Setting entity location updates location") {

		pEntityMock->SetLocation(FVector(1, 2, 3));

		REQUIRE(pEntityMock->GetLocation() == FVector(1, 2, 3));
		REQUIRE(pEntityMock->GetLocation() == FVector(1, 2, 2));

	}

}
