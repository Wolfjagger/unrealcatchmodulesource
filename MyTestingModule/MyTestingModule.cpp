// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#pragma warning (disable:4610)

#define CATCH_CONFIG_RUNNER

#include "MyTestingModule.h"
#include "RequiredProgramMainCPPInclude.h"



IMPLEMENT_APPLICATION(MyTestingModule, "MyTestingModule");



// This runs the stuff that needs TChar rather than char, e.g. the engine initialization.
int32 tchar_main(int32 ArgC, TCHAR* ArgV[]) {

	GEngineLoop.PreInit(ArgC, ArgV);

	return 0;

}

// This is the normal main method. Catch runs in here.
int32 main(int32 ArgC, ANSICHAR* Utf8ArgV[]) {
	
	// Convert from ANSICHAR to TCHAR
	TCHAR** ArgV = new TCHAR*[ArgC];
	for (int32 a = 0; a < ArgC; a++) {
		FUTF8ToTCHAR ConvertFromUtf8(Utf8ArgV[a]);
		ArgV[a] = new TCHAR[ConvertFromUtf8.Length() + 1];
		FCString::Strcpy(ArgV[a], ConvertFromUtf8.Length(), ConvertFromUtf8.Get());
	}

	// Run anything that needs the TChar form of main.
	int32 Result = tchar_main(ArgC, ArgV);

	// Delete TCHAR
	for (int32 a = 0; a < ArgC; a++) {
		delete[] ArgV[a];
	}
	delete[] ArgV;



	// Catch!
	Catch::Session().run(ArgC, Utf8ArgV);



	return Result;

}
