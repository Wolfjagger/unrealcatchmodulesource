// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MyTestingModule : ModuleRules
{
	public MyTestingModule(TargetInfo Target)
	{
		string myUnrealEnginePath = "B:/Unreal/UnrealSource/Engine/Source/";
		string myCorePath = "B:/Unreal/Projects/MyProject/Source/MyCoreModule/";
		PublicIncludePaths.Add(myUnrealEnginePath + "Runtime/Launch/Public");
		PublicIncludePaths.Add(myCorePath);

		PrivateIncludePaths.Add(myUnrealEnginePath + "Runtime/Launch/Private");		// For LaunchEngineLoop.cpp include

		string[] listStringModules = new string[] { "Core", "Projects", "MyCoreModule" };

		PrivateDependencyModuleNames.AddRange(listStringModules);
 
	}
}
